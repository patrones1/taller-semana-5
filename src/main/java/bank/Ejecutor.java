package bank;

import java.util.ArrayList;
import java.util.List;

public class Ejecutor {

    public List<String> moverDinero(Transaccion transaccion){

        List<String> reglasVioladas = new ArrayList<>();

        if (transaccion.cuenta.activa == false){
            reglasVioladas.add("Regla Violada 3");
        }

        if (transaccion.valor > transaccion.cuenta.monto){
            reglasVioladas.add("Regla Violada 4");
        }

        if (transaccion.tipoTransaccion == null){
            reglasVioladas.add("Regla Violada 5");
        }

        if (transaccion.valor > 0){
            if (100000 >= transaccion.valor){

            }
            else {
                reglasVioladas.add("Regla Violada 2");
            }
        }
        else {
            reglasVioladas.add("Regla Violada 1");
        }

        return reglasVioladas;
    }

}
